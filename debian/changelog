maffilter (1.3.1+dfsg-6) unstable; urgency=medium

  * Team upload.
  * Added patch to combine upstream's c++ flags with out own.
  * d/rules: simplified

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 26 Mar 2024 09:59:19 +0100

maffilter (1.3.1+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.6.2 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 13 Aug 2023 12:55:55 +0200

maffilter (1.3.1+dfsg-4) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 24 Nov 2022 12:20:55 +0100

maffilter (1.3.1+dfsg-3) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 12 Oct 2021 09:00:57 +0200

maffilter (1.3.1+dfsg-2) unstable; urgency=medium

  * Install examples in the main package
  * Add autopkgtests
  * Install docs
  * Add Pranav Ballaney to Uploaders
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Pranav Ballaney <ballaneypranav@gmail.com>  Sun, 23 Aug 2020 18:17:16 +0530

maffilter (1.3.1+dfsg-1) unstable; urgency=medium

  [ Julien Dutheil ]
  * New upstream version.

  [ Andreas Tille ]
  * Standards-Version: 4.2.0

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Wed, 22 Aug 2018 11:39:47 +0200

maffilter (1.3.0+dfsg-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * Added ref to OMICtools registry
    (bio.tools and RRID entries not available)

  [ Julien Y. Dutheil ]
  * New upstream version (Closes: #896486).
  * Standards-Version: 4.1.4
  * debhelper 11
  * Updated Vcs-Browser and Vcs-Git
  * Updated Homepage

 -- Julien Dutheil <julien.dutheil@univ-montp2.fr>  Mon, 30 Apr 2018 11:57:36 +0200

maffilter (1.2.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Fix watch file
  * debhelper 10
  * adjust versioned Build-Depends
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Thu, 22 Jun 2017 09:09:21 +0200

maffilter (1.1.0-1+dfsg-2) unstable; urgency=medium

  * Fix permissions
  * Properly install executable and info file

 -- Andreas Tille <tille@debian.org>  Sun, 22 May 2016 21:58:51 +0200

maffilter (1.1.0-1+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #822776)

 -- Andreas Tille <tille@debian.org>  Wed, 27 Apr 2016 15:59:57 +0200
