Source: maffilter
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Julien Dutheil <julien.dutheil@univ-montp2.fr>,
           Pranav Ballaney <ballaneypranav@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               texinfo,
               libbpp-phyl-omics-dev,
               libboost-iostreams-dev,
               zlib1g-dev,
               libbz2-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/maffilter
Vcs-Git: https://salsa.debian.org/med-team/maffilter.git
Homepage: https://jydu.github.io/maffilter/
Rules-Requires-Root: no

Package: maffilter
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: maffilter-examples
Description: process genome alignment in the Multiple Alignment Format
 MafFilter applies a series of "filters" to a MAF file, in order to
 clean it, extract data and computer statistics while keeping track of
 the associated meta-data such as genome coordinates and quality scores.
 .
  * It can process the alignment to remove low-quality / ambiguous /
    masked regions.
  * It can export data into a single or multiple alignment file in
    format such as Fasta or Clustal.
  * It can read annotation data in GFF or GTF format, and extract the
    corresponding alignment.
  * It can perform sliding windows calculations.
  * It can reconstruct phylogeny/genealogy along the genome alignment.
  * It can compute population genetics statistics, such as site
    frequency spectrum, number of fixed/polymorphic sites, etc.

Package: maffilter-examples
Architecture: all
Depends: ${misc:Depends}
Enhances: maffilter
Multi-Arch: foreign
Description: process genome alignment in the Multiple Alignment Format (example data)
 MafFilter applies a series of "filters" to a MAF file, in order to
 clean it, extract data and computer statistics while keeping track of
 the associated meta-data such as genome coordinates and quality scores.
 .
  * It can process the alignment to remove low-quality / ambiguous /
    masked regions.
  * It can export data into a single or multiple alignment file in
    format such as Fasta or Clustal.
  * It can read annotation data in GFF or GTF format, and extract the
    corresponding alignment.
  * It can perform sliding windows calculations.
  * It can reconstruct phylogeny/genealogy along the genome alignment.
  * It can compute population genetics statistics, such as site
    frequency spectrum, number of fixed/polymorphic sites, etc.
 .
 This package provides example data for maffilter.
